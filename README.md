# RYO Aperture

This repository is a reworking of the [RYO Aperture](https://ryomodular.com/APERTURE.html) Eurorack module designed by ljunggren and released as 'PCB and Panel' kits in 2015. Schematics for that module are available at the link above. ljunggren's original module is still available from SynthCube as a 'PCB and Panel' kit, full kit with all necessary components, or even a fully assembled module. I would not have embarked on this endeavor if I did not enjoy the original module; I suggest picking up some version of it.

The original Aperture was a rework of the Buchla 292 Low Pass Gate module. It is an interesting and bizarre 2-pole Sallen-Key low-pass filter. More information than any one person should possibly attempt to digest is available on the Buchla 292, but the following is probably enough to get started working with this project.

Jimmy Kaplan at [Midcentury Modular](https://www.midcentury-modular.com/) has been kind enough to share their low pass gate journey in [a detailed and explainatory blog post](https://www.midcentury-modular.com/blog/lpg). They shared full schematics of their module in that blog post and have been kind enough to answer emails.

[Aaron Lanterman](https://lanterman.ece.gatech.edu/) has included a video on [Buchla Lowpass Gates](https://www.youtube.com/watch?v=NGz3dDnoVVk) in their series of videos which accompany an ECE course they teach at Georgia Tech related to 'Analog Circuits for Music Synthesis'

Doepfer has included a short write up on [Technical details for the A-101-1 Vactrol Multitype Filter](https://www.doepfer.de/a1011_tec.htm) to accompany the epynomous module. This diverges from the traditional LPG in that it also has high pass and band pass, and differs from many state variable filters in that these are inputs to the filter rather than outputs from the filter.

# License

Since ljunggren did not provide a license statement on their schematics, and emails to all available RYO addresses bounce back, I am attempting to honor as much of the original intention of the RYO project as possible while sharing what I've been working on. In general, and this is probably not the place for this, but much of the music synthesis hobby pre-dates more modern copyleft licensing practices, while still following much of the spirit of these practices. Strangely, these differences without distinction collide at times. As such, I've chosen to license this repository as CC-BY-SA-NC in the hopes that this satisfies the intention of ljunggren's RYO efforts while still providing an actual license statement.

See more about [Creative Commons licensing here](https://creativecommons.org/licenses/by-nc-sa/4.0/)